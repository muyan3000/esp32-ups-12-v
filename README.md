# ESP32 UPS NUT支持

#### 介绍
硬件：

基于 CN3306升压充电，TPS40057 12v 10A输出的ups系统，可监测输入，输出，电池的电压及电流，一路可控制输出


软件：

通过esp32监测电压，通过web可查看信息及控制输出，以及断电及恢复供电时通过mail通知用户，通过ssh对linux系统进行关机操作

20250206 

增加NUT功能，可以使QNAP通过 NUT Slave方式获取UPS状态（仅代码片段，未完成完整程序）


———————————————————————————————————————————————

以前买过N个山特c1ks，基本都故障了，不是主机故障就是铅酸电池老化，之后很少遇到断电，就没再配置

但NAS最近遇到电源故障，12V电源挂了，为了NAS的安全，还是需要有个UPS，基于NAS，路由器都是12v供电，所以构建如下

主电源12v，使用LTC4412理想二极管进行电源路径管理，充电部分使用 CN3306升压，电池电压<32V，4串锂电保护电路采用BM3451，放电部分使用kic1210，TPS40057 自带短路保护，40V输入，10A输出自带散热片，实际测试135W稳定工作


硬件部分有2个版本

早期的单颗INA226监测输出功率


电源监控的输出部分使用一片INA226，输入电压和电池电压由ESP32自带的ADC脚粗略检测



https://oshwhub.com/muyan2020/dian-chi-zu-ban_copy_copy_copy

![输入图片说明](images/8PRbw5Jba9hoyGaLNHxxOxNGZ9kik51dOfOSUHiF.png)

![输入图片说明](images/aqshR21igKTdQj28VYjLPPsVmNeQUbplCp9hRkdK.jpeg)


新版INA3221

https://oshwhub.com/muyan2020/dian-chi-zu-ban_copy_copy_copy_copy

![输入图片说明](images/VugEr8US6rYlGl57MJitY427w4o1jPFP8l7NLl8J.jpeg)


主要功能：

电源监控的输出部分由上一版本INA226改为INA3221，三路电压电流监测，12v输入电流监测、电池电流输出监测和12v输出电流监测

一路常开，一路可控输出

增加小屏幕显示当前信息

当断电及恢复时由esp32发送emai提示

![输入图片说明](images/vARQjQiNd5HVF450Tfa52T8SoJQqyFoUiDDuktCi.jpeg)



20240410

终于咬咬牙把这个坑给填了，功能主要基于自己的日常使用，所以有些功能可能不是很适合，需要的先用用看吧，有需要再改进

留个坑整理一下功能说明